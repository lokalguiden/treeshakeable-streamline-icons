### Usage instructions

1. Add the plugin using `yarn add https://gitlab.com/lokalguiden/treeshakeable-streamline-icons`
2. Add the plugin to your `.babelrc`:

```
{
  "plugins": [
    "treeshakeable-streamline-icons"
  ]
}
```
