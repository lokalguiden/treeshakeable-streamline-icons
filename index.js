function plugin({types: t}) {
    return {
        visitor: {
            Identifier(path) {
                // Remove `exports["default"] = _default;`
                if (path.node.name === 'exports') {
                    path.parentPath.parentPath.remove();
                }
            },
            VariableDeclaration(path) {
                if (path.node.declarations[0].id.name !== '_default') {
                    // Only the `_default` variable should be re-written
                    return;
                }

                // As the automatically inferred TypeScript types aren't precise enough to be used with
                // `streamline-wrapper-react` inject the actual types.
                t.addComment(path.node, 'leading', `* @typedef ${ICON_TYPESCRIPT_TYPEDEF} `, false);

                // Extract each key in the object to its own `export const` where the constant name
                // is the key in the object. This allows the icon to be tree-shaked.
                for (const iconNode of path.node.declarations[0].init.properties) {
                    const identifierName = createExportableIdentifierName(iconNode);
                    const variableDeclaration = t.variableDeclaration(
                        'var',
                        [
                            t.variableDeclarator(
                                t.identifier(identifierName),
                                iconNode.value
                            )
                        ]
                    );

                    const exportDeclaration = t.exportNamedDeclaration(
                        variableDeclaration,
                        undefined
                    );

                    t.addComment(exportDeclaration, 'leading', `* @type ${ICON_TYPESCRIPT_TYPE_NAME} `, false);

                    path.insertAfter(exportDeclaration);
                }

                // As all keys in the object has been extracted to their own constants the original
                // variable can be removed.
                path.remove();
            }
        }
    };
}

function createExportableIdentifierName(node) {
    let identifierName;
    if (node.key.type === 'Identifier') {
        identifierName = node.key.name;
    } else if (node.key.type === 'StringLiteral') {
        identifierName = node.key.value;
    } else {
        throw new Exception(`Unexpected object key node type: ${node.key.type}`);
    }

    // Some icons have names that aren't valid variable names, such as `3DPrintTshirt`.
    // Prepending `Icon` to the name yields a valid variable name.
    return `Icon${identifierName}`;
}

const ICON_TYPESCRIPT_TYPEDEF = "{[string, number, number, {fill: string;stroke: string;'stroke-linecap': 'butt'|'round'|'square'|'inherit';'stroke-linejoin': 'miter'|'round'|'bevel'|'inherit';'stroke-width': number|string;}[], string[]]} Icon";
const ICON_TYPESCRIPT_TYPE_NAME = "Icon";

module.exports = plugin;
